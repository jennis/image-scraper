#!/usr/bin/env python3

# download_images.py
# Given a query, download a specified number of images from google

import argparse
import os
import shutil
import sys

import cv2
import requests
from selenium import webdriver

from get_query_image_urls import get_query_image_urls


# Parse the arguments
ap = argparse.ArgumentParser()
ap.add_argument("-q", "--query", type=str, required=True,
                help="The google search string")
ap.add_argument("-n", "--images", type=int, default=10,
                help="The number of images to download")
ap.add_argument("-w", "--web-driver", required=True,
                help="Path to the webdriver\n \
                For chrome, see: https://chromedriver.chromium.org/downloads")
ap.add_argument("-o", "--output", required=True,
                help="Path to output directory of images")
args = vars(ap.parse_args())


# Determine the driver path
if os.path.isabs(args["web_driver"]):
    DRIVER_PATH = args["web_driver"]
else:
    # Assume its relative...
    DRIVER_PATH = os.path.join(os.getcwd(), args["web_driver"])


# Check the output directory
if os.path.exists(args["output"]):
    print(f"[INFO] {args['output']} already exists. Do you wish to overwrite it?")
    response = ""
    while response not in ['Y', 'N', 'y', 'n']:
        response = input("Please enter [Y] or [N]: ")

    # If yes, remove then create a fresh directory
    if response in ['Y', 'y']:
        print(f"[INFO] Removing the contents of {args['output']}")
        shutil.rmtree(args["output"])
        os.mkdir(args["output"])
    else:
        print("[INFO] Aborting...")
        sys.exit()
else:
    print(f"[INFO] Creating new directory: {args['output']}")
    os.mkdir(args["output"])

# Get the image urls of the query
# Note that the third argument here is the webdriver which we initiate
urls = get_query_image_urls(args["query"],
                            args["images"],
                            webdriver.Chrome(executable_path=DRIVER_PATH),
                            sleep_between_interactions=0)

# Loop over the urls and download each one
total = 0
imagePaths = []
for url in urls:
    try:
        # Try to download the image
        r = requests.get(url, timeout=60)

        # Save the image
        p = os.path.join(str(args["output"]), "{}.jpg".format(str(total).zfill(8)))
        with open(p, "wb") as f:
            f.write(r.content)

        # Update the counter and save the path
        print("[INFO] downloaded: {}".format(p))
        imagePaths.append(p)
        total += 1

    except:
        # If there was a problem, just notify and move on
        print("[INFO] error downloading {}...skipping".format(url))
        continue

# Loop over all of the downloaded images and if there is
# a problem opening them with OpenCV we'll just delete them
print("[INFO] Verifying images have downloaded and are not corrupt")
for imagePath in imagePaths:
    delete = False

    # Try to load the image
    try:
        image = cv2.imread(imagePath)

        # If the image is `None` then we could not properly load it
        if image is None:
            print("None")
            delete = True

    # If OpenCV cannot load the image then the image is likely
    # corrupt so we should delete it
    except:
        print("Except")
        delete = True

    # Delete if necessary
    if delete:
        print("[INFO] deleting {}".format(imagePath))
        os.remove(imagePath)

print("[INFO] Done :)")
