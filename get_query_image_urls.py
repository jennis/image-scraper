# NOTE: For this script to work, you will need the relevant ChromeDriver,
# specific to your OS and your version of chrome, see:
# https://chromedriver.chromium.org/downloads (make sure you unzip it and put
# the executable in the relevant directory)

import os
import time

# A bit of foreword about the selenium package: Most modern webpages are
# interactive, i.e. the page itself changes without redirecting to another page
# etc. And this interactivity happens on certain user interactions, e.g.
# clicking, scrolling etc. Selenium will make the webpage think we're doing
# that
#
# NOTE: It is possible to use Selenium with other browsers, you'll just have to
# use the respective web drivers
from selenium import webdriver


def get_query_image_urls(query:str, max_links_to_fetch:int,
                     wd:webdriver, sleep_between_interactions:int=1):
    def scroll_to_end(wd):
        wd.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(sleep_between_interactions)

    # Build the google query
    SEARCH_URL = "https://www.google.com/search?safe=off&site=&tbm=isch&source=hp&q={q}&oq={q}&gs_l=img"

    # Load the page
    wd.get(SEARCH_URL.format(q=query))

    image_urls = set()
    image_count = 0
    results_start = 0
    while image_count < max_links_to_fetch:
        scroll_to_end(wd)

        # Get all image thumbnail results
        thumbnail_results = wd.find_elements_by_css_selector("img.Q4LuWd")
        number_results = len(thumbnail_results)

        print(f"Found: {number_results} search results. Extracting links from \
        {results_start}:{number_results}")

        for img in thumbnail_results[results_start:number_results]:
            # Try to click every thumbnail such that we can get the real image
            # behind it
            try:
                img.click()
                time.sleep(sleep_between_interactions)
            except:
                # Just continue if something goes wrong with the click
                continue

            # Extract image urls
            actual_images = wd.find_elements_by_css_selector('img.n3VNCb')
            for actual_image in actual_images:
                if actual_image.get_attribute('src') and 'http' in actual_image.get_attribute('src'):
                    image_urls.add(actual_image.get_attribute('src'))

            # Update the counter
            image_count = len(image_urls)
            if image_count >= max_links_to_fetch:
                print(f"Found: {len(image_urls)} image links, done!")
                break
        else:
            # Load more...
            print("Found:", image_count, "image links, looking for more ...")
            time.sleep(30)
            return
            load_more_button = wd.find_element_by_css_selector(".mye4qd")
            if load_more_button:
                wd.execute_script("document.querySelector('.mye4qd').click();")

        # Move the result startpoint further down
        results_start = len(thumbnail_results)

    return image_urls
