# Pulling a bunch of images from Google

This is a small script which scrapes a load of images from google.

To do this, we first need to obtain a list of image urls from a search query.
This has been done using the Selenium package which simulates web browser
activity. See [get_query_image_urls.py](get_query_image_urls.py) for
implementation.

Once we have a list of image urls, we need to download them. We simply loop over
the url list and use Python's requests library to try and _get_ each image. If
anything goes wrong, we just move on. Once we have the image, we save it with a
number to the specified output directory. Once we've downloaded all the images
we try to load each and every one of them with OpenCV, if anything goes wrong,
we just assume that the image is corrupt and delete the image.

## Usage
```
python download_images.py --query "What you want to search" --images $no_of_images_to_download --web-driver $path_to_webdriver, --output $output_dir
```
